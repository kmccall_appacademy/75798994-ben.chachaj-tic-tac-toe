class Board
  attr_reader :grid, :marks
  def initialize(grid = [[nil, nil, nil],
                         [nil, nil, nil],
                         [nil, nil, nil]])
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[-1]] = mark
  end

  def empty?(pos)
    return true if grid[pos[0]][pos[-1]] == nil
  end

  def winner
    if grid[0].all? {|x| x == :X }
      return :X
    elsif grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
      :X
    elsif grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
      :X
    elsif column_winner(grid)
      :O
    else
      nil
    end
  end

  def [](row, col)
    @grid[row][col]
  end

  def []=(row, col, mark)
    @grid[row][col] = mark
  end

  def column_winner(grid)
    cols = grid.transpose
    cols.any? do |col|
      col.all? { |el| el == :O }
    end
  end

  def over?
   grid.flatten.none? { |pos| pos.nil? } || winner
  end
end
