class HumanPlayer
  attr_reader :name
   def initialize(name)
     @name = name
   end

   def get_move
     puts "where move:"
     input = gets.chomp
     pos = input.split(",").map(&:to_i)
     #pos.map! { |el| el.to_i }
   end

   def display(board)
    puts "    0   1   2  "
    puts "  |-----------|"
    puts board.grid[0]
    puts "  |-----------|"
    puts board.grid[1]
    puts "  |-----------|"
    puts board.grid[2]
    puts "  |-----------|"
  end
end

# if __FILE__ == $PROGRAM_NAME
#   t = HumanPlayer.new("jim")
#
#   t.get_move
#
# end

# HumanPlayer
#   #initialize
#     initializes with a name (FAILED - 1)
#   #get_move
#     asks for a move (FAILED - 2)
#     should take an entry of the form '0, 0' and return a position (FAILED - 3)
#   #display
#     prints the board to the screen (FAILED - 4)
